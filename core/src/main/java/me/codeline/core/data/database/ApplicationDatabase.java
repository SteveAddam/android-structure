package me.codeline.core.data.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import me.codeline.core.data.database.product.Product;
import me.codeline.core.data.database.dao.FavoriteDao;

@Database(entities = {Product.class},
        version = 1,
        exportSchema = false)
public abstract class ApplicationDatabase extends RoomDatabase {

    private static final Object LOCK = new Object();

    private static ApplicationDatabase sInstance;
    private static final String DATABASE_NAME = "ApplicationDatabase";

    public static ApplicationDatabase getInstance(Context context){

        if(sInstance == null){
            synchronized (LOCK){
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        ApplicationDatabase.class, ApplicationDatabase.DATABASE_NAME)
                        .build();
            }
        }
        return sInstance;
    }

    public abstract FavoriteDao favoriteDao();

}
