package me.codeline.core.data.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import me.codeline.core.data.database.product.Product;

@Dao
public interface FavoriteDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertFavorite(List<Product> favorites);

    @Query("SELECT * From product WHERE favourite = 1")
    LiveData<List<Product>> getFavorites();

    @Query("UPDATE product SET favourite = 1 WHERE id = :productId")
    void setFavorite(int productId);

    @Query("UPDATE product SET favourite = 0 WHERE id = :productId")
    void unSetFavorite(int productId);
}
