package me.codeline.core.data.viewmodel;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

public class BaseListViewModel extends BaseViewModel {

    private boolean hasMore = true;
    private int page = 1;
    private boolean loading = false;

    public boolean hasMore() {
        return hasMore;
    }

    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }

    @Bindable
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
        notifyChange(BR.page);
    }

    public void increasePage(){
        this.page+= 1;
    }
    @Bindable
    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
        notifyChange(BR.loading);
    }

    public void reset(){
        this.hasMore = true;
        this.page = 1;
    }
}
