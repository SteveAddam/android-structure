package me.codeline.core.data.database.product;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;


@Entity(tableName = "product")
public class Product extends BaseProduct implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("serving_size")
    private String servingSize;
    @SerializedName("serving_unit")
    private String servingUnit;
    @SerializedName("price")
    private double price;
    @SerializedName("discounted_price")
    private double discountedPrice;
    @SerializedName("group_quantity")
    private String groupQuantity;
    @SerializedName("group_description")
    private String groupDescription;
    @SerializedName("sort_order")
    private int sortOrder;
    @SerializedName("status")
    private int status;
    @SerializedName("store_id")
    private int storeId;
    @SerializedName("favourite")
    private boolean favourite;
    @SerializedName("in_stock")
    private boolean inStock;
    @SerializedName("new")
    private boolean newX;

    @Ignore
    private boolean checked = false;
    @Ignore
    private boolean hasDiscount=false;
    @Ignore
    private int inCart = 0;

    @Ignore
    @SerializedName("in_list")
    private boolean inList =false;

    public Product(int id, String title, String description, String servingSize, String servingUnit, Double price, Double discountedPrice, String groupQuantity, String groupDescription, int sortOrder, int status, int storeId, boolean favourite, boolean inStock, boolean newX,boolean inList) {
        super(TYPE_NORMAL);
        this.id = id;
        this.title = title;
        this.description = description;
        this.servingSize = servingSize;
        this.servingUnit = servingUnit;
        this.price = price;
        this.discountedPrice = discountedPrice;
        this.groupQuantity = groupQuantity;
        this.groupDescription = groupDescription;
        this.sortOrder = sortOrder;
        this.status = status;
        this.storeId = storeId;
        this.favourite = favourite;
        this.inStock = inStock;
        this.newX = newX;
        this.inList = inList;
    }
    public Product() {
        super(TYPE_EMPTY);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getServingSize() {
        return servingSize;
    }

    public void setServingSize(String servingSize) {
        this.servingSize = servingSize;
    }

    public String getServingUnit() {
        return servingUnit;
    }

    public void setServingUnit(String servingUnit) {
        this.servingUnit = servingUnit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public String getGroupQuantity() {
        return groupQuantity;
    }

    public void setGroupQuantity(String groupQuantity) {
        this.groupQuantity = groupQuantity;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }

    public boolean isNewX() {
        if(newX && inStock){
            return true;
        }
        return false;
    }

    public void setNewX(boolean newX) {
        this.newX = newX;
    }

    @Bindable
    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
        notifyPropertyChanged(BR.checked);

    }

    public boolean isHasDiscount() {
        return getDiscountedPrice()>0;
    }

    public void setHasDiscount(boolean hasDiscount) {
        this.hasDiscount = hasDiscount;
    }

    @Bindable
    public int getInCart() {
        return inCart;
    }

    public void setInCart(int inCart) {
        this.inCart = inCart;
        notifyPropertyChanged(BR.inCart);
    }

    public boolean isInList() {
        return inList;
    }

    public void setInList(boolean inList) {
        this.inList = inList;
    }

    @Ignore
    public String getSizeString(){
        String size = "";
        if (getServingUnit() != null && getServingSize() != null && !getServingSize().equals("0")) {
            size += getServingSize() + getServingUnit() + " ";
        }
        if (getGroupQuantity() != null && getGroupDescription() != null
                && !getGroupQuantity().equals("0") && !getGroupDescription().equals("")) {
            size += getGroupQuantity() + " " + getGroupDescription();
        }
        return size;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.servingSize);
        dest.writeString(this.servingUnit);
        dest.writeDouble(this.price);
        dest.writeDouble(this.discountedPrice);
        dest.writeString(this.groupQuantity);
        dest.writeString(this.groupDescription);
        dest.writeInt(this.sortOrder);
        dest.writeInt(this.status);
        dest.writeInt(this.storeId);
        dest.writeByte(this.favourite ? (byte) 1 : (byte) 0);
        dest.writeByte(this.inStock ? (byte) 1 : (byte) 0);
        dest.writeByte(this.newX ? (byte) 1 : (byte) 0);
        dest.writeByte(this.checked ? (byte) 1 : (byte) 0);
        dest.writeByte(this.hasDiscount ? (byte) 1 : (byte) 0);
        dest.writeInt(this.inCart);
        dest.writeByte(this.inList ? (byte) 1 : (byte) 0);
    }

    protected Product(Parcel in) {
        super(in);
        this.id = in.readInt();
        this.title = in.readString();
        this.description = in.readString();
        this.servingSize = in.readString();
        this.servingUnit = in.readString();
        this.price = in.readDouble();
        this.discountedPrice = in.readDouble();
        this.groupQuantity = in.readString();
        this.groupDescription = in.readString();
        this.sortOrder = in.readInt();
        this.status = in.readInt();
        this.storeId = in.readInt();
        this.favourite = in.readByte() != 0;
        this.inStock = in.readByte() != 0;
        this.newX = in.readByte() != 0;
        this.checked = in.readByte() != 0;
        this.hasDiscount = in.readByte() != 0;
        this.inCart = in.readInt();
        this.inList = in.readByte() != 0;
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
