package me.codeline.core.data.viewmodel;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.Observable;
import androidx.databinding.PropertyChangeRegistry;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


public class BaseViewModel extends ViewModel implements Observable {

    private PropertyChangeRegistry callBacks = new PropertyChangeRegistry();
    private MutableLiveData<ApiState> apiState = new MutableLiveData<>();

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        callBacks.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        callBacks.remove(callback);
    }

    public void notifyChange() {
        callBacks.notifyChange(this, 0);
    }

    public void notifyChange(int id) {
        callBacks.notifyChange(this, id);
    }


    public class ApiState extends BaseObservable {
        private boolean loading = true;
        private Exception exception = null;

        public ApiState(boolean loading, Exception exception) {
            this.setLoading(loading);
            this.setException(exception);
        }

        @Bindable
        public boolean isLoading() {
            return loading;
        }

        public void setLoading(boolean loading) {
            this.loading = loading;
            notifyPropertyChanged(BR.loading);
        }

        public Exception getException() {
            return exception;
        }

        public void setException(Exception exception) {
            this.exception = exception;
        }
    }

    public void updateApiState(boolean loading, Exception e) {
        if (apiState.getValue() != null) {
            apiState.getValue().setLoading(loading);
            apiState.getValue().setException(e);
            apiState.postValue(apiState.getValue());
        } else {
            apiState.postValue(new ApiState(loading, e));
        }
        notifyChange(BR.apiState);
        notifyChange(BR.loading);
    }

    @Bindable
    public MutableLiveData<ApiState> getApiState() {
        return apiState;
    }

}
