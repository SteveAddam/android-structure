package me.codeline.core.data.model.response;

public class ApiResponse<T> {

    private boolean success;
    private Exception exception;
    private T data;
    private BaseResponse<T> baseResponse;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public BaseResponse<T> getBaseResponse() {
        return baseResponse;
    }

    public void setBaseResponse(BaseResponse<T> baseResponse) {
        this.baseResponse = baseResponse;
    }
}
