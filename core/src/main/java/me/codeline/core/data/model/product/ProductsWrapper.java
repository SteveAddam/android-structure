package me.codeline.core.data.model.product;

import java.util.List;

import me.codeline.core.data.database.product.Product;
import me.codeline.core.data.model.response.BaseResponse;

public class ProductsWrapper extends BaseResponse<List<Product>> {
}
