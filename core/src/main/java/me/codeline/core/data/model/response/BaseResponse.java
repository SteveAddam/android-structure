package me.codeline.core.data.model.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseResponse<T> implements Serializable {


    private static final long serialVersionUID = -91500570L;

    /**
     * Boolean that returns weather the request executed successfully without errors or not. If false
     * an {@link Error} object is returned
     */
    @SerializedName("success")
    private boolean success;

    /**
     * This is an object of type T. Requested data are found inside this object
     */
    @SerializedName("data")
    private T data;

    /**
     * Current page the response is at
     */
    @SerializedName("current_page")
    private int currentPage;

    /**
     * Url to the first page of this request
     */
    @SerializedName("first_page_url")
    private String firstPageUrl;


    /**
     * Last page number
     */
    @SerializedName("last_page")
    private int lastPage;

    /**
     * Last page url for this request
     */
    @SerializedName("last_page_url")
    private String lastPageUrl;

    /**
     * Next page url to call. Example if this request is at page 1, the url would be for page 2
     */
    @SerializedName("next_page_url")
    private String nextPageUrl;

    /**
     * Path to this request without any parameters appended
     */
    @SerializedName("path")
    private String path;

    /**
     * How many items per page
     */
    @SerializedName("per_page")
    private int perPage;

    /**
     * Previous page url
     */
    @SerializedName("prev_page_url")
    private String prevPageUrl;

    /**
     * Starting index of the items returned
     */
    @SerializedName("from")
    private int from;

    /**
     * Ending index of items returned
     */
    @SerializedName("to")
    private int to;

    /**
     * Total number of items available to paginate
     */
    @SerializedName("total")
    private int total;


    /**
     * Empty constructor
     */
    public BaseResponse() {
    }


    /**
     * Getters and setters
     */

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getFirstPageUrl() {
        return firstPageUrl;
    }

    public void setFirstPageUrl(String firstPageUrl) {
        this.firstPageUrl = firstPageUrl;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public String getLastPageUrl() {
        return lastPageUrl;
    }

    public void setLastPageUrl(String lastPageUrl) {
        this.lastPageUrl = lastPageUrl;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getPrevPageUrl() {
        return prevPageUrl;
    }

    public void setPrevPageUrl(String prevPageUrl) {
        this.prevPageUrl = prevPageUrl;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
