package me.codeline.core.data.database.product;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.databinding.BaseObservable;

public class BaseProduct extends BaseObservable implements Parcelable {
    public static final int TYPE_NORMAL =1;
    public static final int TYPE_HEADER =2;
    public final static int TYPE_EMPTY = 3;

    private int type = TYPE_NORMAL;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public BaseProduct(int type) {
        this.type = type;
    }

    public BaseProduct(Parcel in) {
        this.type = in.readInt();
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.type);

    }

}
