package me.codeline.core.data.source;

import androidx.lifecycle.MutableLiveData;

import me.codeline.core.data.model.response.ApiResponse;
import me.codeline.core.data.model.response.BaseResponse;

public class BaseDataSource {

    public <T> void setSuccess(ApiResponse<T> apiResponse, MutableLiveData<ApiResponse<T>> liveData, BaseResponse<T> response) {
        apiResponse.setSuccess(true);
        apiResponse.setData(response.getData());
        apiResponse.setBaseResponse(response);
        liveData.postValue(apiResponse);
    }

    public <T> void setError(ApiResponse<T> apiResponse, MutableLiveData<ApiResponse<T>> liveData, Exception e) {
        apiResponse.setSuccess(false);
        apiResponse.setException(e);
        liveData.postValue(apiResponse);
    }
}
