package me.codeline.core.util;


import androidx.lifecycle.Observer;

import me.codeline.core.data.model.response.ApiResponse;

public abstract class ApiObserver<T> implements Observer<ApiResponse<T>> {
    @Override
    public void onChanged(ApiResponse<T> apiResponse) {
        if (apiResponse.isSuccess()) {
            onSuccess(apiResponse.getData());
        } else {
            onError(apiResponse.getException());
        }
    }

    public abstract void onSuccess(T data);

    public abstract void onError(Exception e);
}
