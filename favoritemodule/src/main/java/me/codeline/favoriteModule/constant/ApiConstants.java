package me.codeline.favoriteModule.constant;

public class ApiConstants {


    public static final String BASE = "https://api.markitapp.me/";
    public static final String POST_FAVORITE = BASE + "api/v1/product/favorite";
    public static final String DELETE_MULTIPLE_FAVORITES = BASE + "api/v1/user/product/favorite/multiple";
    public static final String GET_FAVORITES = BASE + "api/v1/user/product/favorite/list";
}
