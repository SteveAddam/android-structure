package me.codeline.favoriteModule.data.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import java.util.List;

import me.codeline.core.data.model.response.ApiResponse;
import me.codeline.core.data.database.product.Product;

public class FavoriteRepository {

    private FavoriteCacheDataRepository mFavoriteCacheDataRepository;
    private FavoriteRemoteRepository mFavoriteRemoteRepository;

    private static FavoriteRepository sInstance;
    private static final Object LOCK = new Object();

    private FavoriteRepository(FavoriteCacheDataRepository mFavoriteCacheDataRepository, FavoriteRemoteRepository mFavoriteRemoteRepository) {
        this.mFavoriteCacheDataRepository = mFavoriteCacheDataRepository;
        this.mFavoriteRemoteRepository = mFavoriteRemoteRepository;
        observeProducts();
    }

    public static FavoriteRepository getInstance(FavoriteCacheDataRepository mFavoriteCacheDataRepository, FavoriteRemoteRepository mFavoriteRemoteRepository) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new FavoriteRepository(mFavoriteCacheDataRepository, mFavoriteRemoteRepository);
            }
        }
        return sInstance;
    }

    private void observeProducts(){
        LiveData<List<Product>> products = mFavoriteRemoteRepository.getProducts();
        products.observeForever(new Observer<List<Product>>() {
            @Override
            public void onChanged(List<Product> products) {
                mFavoriteCacheDataRepository.insertProducts(products);
            }
        });
    }

    public MutableLiveData<ApiResponse<String>> callPostFavorite(int productId, boolean status){
       return mFavoriteRemoteRepository.postFavorites(productId, status);
    }

    public MutableLiveData<ApiResponse<List<Product>>> callGetFavorites(int page){
        return mFavoriteRemoteRepository.getFavorites(page);
    }

    public MutableLiveData<ApiResponse<String>> callDeleteFavorites(List<Integer> ids){
        return mFavoriteRemoteRepository.deleteFavorites(ids);
    }


}
