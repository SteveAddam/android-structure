package me.codeline.favoriteModule.data.viewmodel;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import me.codeline.favoriteModule.data.repository.FavoriteRepository;
import me.codeline.favoriteModule.util.InjectorUtils;

public class FavoriteViewModelFactory implements ViewModelProvider.Factory {

    private FavoriteRepository favoriteRepository;

    public FavoriteViewModelFactory(Context context) {
        this.favoriteRepository = InjectorUtils.provideFavoriteRepository(context);
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(FavoriteViewModel.class)) {
            return (T) new FavoriteViewModel(favoriteRepository);
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
