package me.codeline.favoriteModule.data.source.cache;

import androidx.lifecycle.LiveData;

import java.util.List;

import me.codeline.core.data.database.ApplicationDatabase;
import me.codeline.core.data.database.product.Product;
import me.codeline.core.util.AppExecutors;

public class CacheDataSource {

    private AppExecutors mExecutor;
    private ApplicationDatabase favoriteDatabase;

    private static final Object LOCK = new Object();
    private static CacheDataSource sInstance;

    private CacheDataSource(AppExecutors mExecutor, ApplicationDatabase favoriteDatabase) {
        this.mExecutor = mExecutor;
        this.favoriteDatabase = favoriteDatabase;
    }

    public static CacheDataSource getInstance(AppExecutors executors, ApplicationDatabase favoriteDatabase) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new CacheDataSource(executors, favoriteDatabase);
            }
        }
        return sInstance;
    }

    public void insertFavorites(final List<Product> favorites){
        mExecutor.getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                favoriteDatabase.favoriteDao().insertFavorite(favorites);
            }
        });
    }

    public LiveData<List<Product>> getFavorites() {
        return favoriteDatabase.favoriteDao().getFavorites();
    }

    public void setFavorite(final int productId){
        mExecutor.getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                favoriteDatabase.favoriteDao().setFavorite(productId);
            }
        });
    }

    public void unSetFavorite(final int productId){
        mExecutor.getDiskIO().execute(new Runnable() {
            @Override
            public void run() {
                favoriteDatabase.favoriteDao().setFavorite(productId);
            }
        });
    }
}
