package me.codeline.favoriteModule.data.source.remote;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import me.codeline.core.data.source.BaseDataSource;
import me.codeline.favoriteModule.manager.FavoriteNetworkManager;
import me.codeline.core.data.model.response.ApiResponse;
import me.codeline.core.data.model.message.MessageWrapper;
import me.codeline.core.data.database.product.Product;
import me.codeline.core.data.model.product.ProductsWrapper;
import me.codeline.core.util.AppExecutors;
import me.codeline.library.core.manager.CLNetworkManager;

public class RemoteDataSource extends BaseDataSource {

    private FavoriteNetworkManager mFavoriteNetworkManager;
    private AppExecutors mAppExecutors;

    private static final Object LOCK = new Object();
    private static RemoteDataSource sInstance;

    private final MutableLiveData<List<Product>> products;

    private RemoteDataSource(FavoriteNetworkManager mFavoriteNetworkManager, AppExecutors mAppExecutors) {
        this.mFavoriteNetworkManager = mFavoriteNetworkManager;
        this.mAppExecutors = mAppExecutors;
        this.products = new MutableLiveData<>();
    }

    public LiveData<List<Product>> getProducts() {
        return products;
    }

    public static RemoteDataSource getInstance(FavoriteNetworkManager favoriteNetworkManager, AppExecutors executors) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new RemoteDataSource(favoriteNetworkManager, executors);
            }
        }
        return sInstance;
    }



    public MutableLiveData<ApiResponse<String>> postFavorite(final int productId, final boolean favorite) {
        final MutableLiveData<ApiResponse<String>> liveData = new MutableLiveData<>();
        final ApiResponse<String> apiResponse = new ApiResponse<>();

        mAppExecutors.getNetworkIO().execute(new Runnable() {
            @Override
            public void run() {
                mFavoriteNetworkManager.postFavorite(productId, favorite, new CLNetworkManager.Listener<MessageWrapper>() {
                    @Override
                    public void onSuccess(MessageWrapper response) {
                        setSuccess(apiResponse, liveData, response);
                        //todo update product
                    }

                    @Override
                    public void onError(Exception e) {
                        setError(apiResponse, liveData, e);
                    }
                });
            }
        });
        return liveData;
    }


    public MutableLiveData<ApiResponse<List<Product>>> getFavorite(final int page) {
        final MutableLiveData<ApiResponse<List<Product>>> liveData = new MutableLiveData<>();
        final ApiResponse<List<Product>> apiResponse = new ApiResponse<>();
        mAppExecutors.getNetworkIO().execute(new Runnable() {
            @Override
            public void run() {
                mFavoriteNetworkManager.getFavorites(page, new CLNetworkManager.Listener<ProductsWrapper>() {
                    @Override
                    public void onSuccess(ProductsWrapper response) {
                        setSuccess(apiResponse, liveData, response);
                        products.postValue(response.getData());
                    }

                    @Override
                    public void onError(Exception e) {
                        setError(apiResponse, liveData, e);
                    }
                });
            }
        });
        return liveData;
    }

    public MutableLiveData<ApiResponse<String>> callPostDeleteFavorites(final List<Integer> ids) {
        final MutableLiveData<ApiResponse<String>> liveData = new MutableLiveData<>();
        final ApiResponse<String> apiResponse = new ApiResponse<>();
        mAppExecutors.getNetworkIO().execute(new Runnable() {
            @Override
            public void run() {
                mFavoriteNetworkManager.deleteFavorites(ids, new CLNetworkManager.Listener<MessageWrapper>() {
                    @Override
                    public void onSuccess(MessageWrapper response) {
                        setSuccess(apiResponse, liveData, response);
                    }

                    @Override
                    public void onError(Exception e) {
                        setError(apiResponse, liveData, e);
                    }
                });
            }
        });
        return liveData;
    }


}
