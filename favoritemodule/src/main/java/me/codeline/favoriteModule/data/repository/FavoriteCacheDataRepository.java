package me.codeline.favoriteModule.data.repository;

import androidx.lifecycle.LiveData;

import java.util.List;

import me.codeline.favoriteModule.data.source.cache.CacheDataSource;
import me.codeline.core.data.database.product.Product;

public class FavoriteCacheDataRepository {

    private CacheDataSource mCacheDataSource;
    private static FavoriteCacheDataRepository sInstance;
    private static final Object LOCK = new Object();


    private FavoriteCacheDataRepository(CacheDataSource mCacheDataSource) {
        this.mCacheDataSource = mCacheDataSource;
    }

    public static FavoriteCacheDataRepository getInstance(CacheDataSource mCacheDataSource) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new FavoriteCacheDataRepository(mCacheDataSource);
            }
        }
        return sInstance;
    }

    public void insertProducts(List<Product> products){
        mCacheDataSource.insertFavorites(products);
    }

    public LiveData<List<Product>> getFavorites(){
        return mCacheDataSource.getFavorites();
    }

    public void setFavorite(int productId){
        mCacheDataSource.setFavorite(productId);
    }

    public void unSetFavorite(int productId){
        mCacheDataSource.unSetFavorite(productId);
    }
}
