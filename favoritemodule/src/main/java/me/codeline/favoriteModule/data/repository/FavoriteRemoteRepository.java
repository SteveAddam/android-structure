package me.codeline.favoriteModule.data.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import me.codeline.core.data.model.response.ApiResponse;
import me.codeline.core.data.database.product.Product;
import me.codeline.favoriteModule.data.source.cache.remote.RemoteDataSource;

public class FavoriteRemoteRepository {

    private RemoteDataSource mRemoteDataSource;
    private static FavoriteRemoteRepository sInstance;
    private static final Object LOCK = new Object();

    private FavoriteRemoteRepository(RemoteDataSource mRemoteDataSource) {
        this.mRemoteDataSource = mRemoteDataSource;
    }

    public static FavoriteRemoteRepository getInstance(RemoteDataSource mRemoteDataSource) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new FavoriteRemoteRepository(mRemoteDataSource);
            }
        }
        return sInstance;
    }

    public LiveData<List<Product>> getProducts() {
        return mRemoteDataSource.getProducts();
    }
    public MutableLiveData<ApiResponse<String>> postFavorites(int productId, boolean status) {
        return mRemoteDataSource.postFavorite(productId, status);
    }

    public MutableLiveData<ApiResponse<List<Product>>> getFavorites(int page) {
        return mRemoteDataSource.getFavorite(page);
    }

    public MutableLiveData<ApiResponse<String>> deleteFavorites(List<Integer> ids) {
        return mRemoteDataSource.callPostDeleteFavorites(ids);
    }

}
