package me.codeline.favoriteModule.data.viewmodel;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import me.codeline.core.data.viewmodel.BaseListViewModel;
import me.codeline.favoriteModule.data.repository.FavoriteRepository;
import me.codeline.core.data.model.response.ApiResponse;
import me.codeline.core.data.database.product.Product;
import me.codeline.core.util.ApiObserver;

public class FavoriteViewModel extends BaseListViewModel {

    private FavoriteRepository favoriteRepository;
    private LiveData<ApiResponse<List<Product>>> favoriteResults;
    private MutableLiveData<List<Product>> favorites = new MutableLiveData<>();
    private boolean showEmptyState;

    public FavoriteViewModel(FavoriteRepository favoriteRepository) {
        this.favoriteRepository = favoriteRepository;
        callGetFavorites();
    }

    private LiveData<ApiResponse<List<Product>>> callGetFavorites() {
        if (favoriteRepository != null) {
            updateApiState(true, null);
            this.favoriteResults = favoriteRepository.callGetFavorites(getPage());
            observeResult();
            return favoriteResults;
        }
        return new MutableLiveData<>();
    }

    private void observeResult() {
        favoriteResults.observeForever(abiObserver);
    }

    private ApiObserver<List<Product>> abiObserver = new ApiObserver<List<Product>>() {
        @Override
        public void onSuccess(List<Product> data) {
            updateApiState(false, null);
            favorites.postValue(data);
            unregister();
        }

        @Override
        public void onError(Exception e) {
            updateApiState(false, e);
            unregister();
        }
    };

    @Override
    public void increasePage() {
        super.increasePage();
        this.favoriteResults = callGetFavorites();
    }

    public MutableLiveData<List<Product>> getFavorites() {
        return favorites;
    }

    private void unregister() {
        favoriteResults.removeObserver(abiObserver);
    }

    public void refresh() {
        this.reset();
        this.favoriteResults = callGetFavorites();
        observeResult();
    }

    public LiveData<ApiResponse<String>> callDeleteFavorite(List<Integer> productIds) {
        if (favoriteRepository != null) {
            return favoriteRepository.callDeleteFavorites(productIds);
        }
        return new MutableLiveData<>();
    }

    @Bindable
    public boolean isShowEmptyState() {
        return showEmptyState;
    }

    public void setShowEmptyState(boolean showEmptyState) {
        this.showEmptyState = showEmptyState;
        notifyChange(BR.showEmptyState);
    }
}
