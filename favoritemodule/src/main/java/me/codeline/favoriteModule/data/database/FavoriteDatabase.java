package me.codeline.favoriteModule.data.database;

import android.content.Context;

import androidx.room.Room;

import me.codeline.core.data.database.ApplicationDatabase;


public abstract class FavoriteDatabase {

    private static final Object LOCK = new Object();

    private static ApplicationDatabase sInstance;

    public static ApplicationDatabase getInstance(Context context){
        if(sInstance == null){
            synchronized (LOCK){
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        ApplicationDatabase.class, "ApplicationDatabase")
                        .build();
            }
        }
        return sInstance;
    }
}
