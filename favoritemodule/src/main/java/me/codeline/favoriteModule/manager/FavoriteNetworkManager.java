package me.codeline.favoriteModule.manager;

import android.content.Context;

import com.android.volley.Request;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import me.codeline.favoriteModule.constant.ApiConstants;
import me.codeline.core.data.model.message.MessageWrapper;
import me.codeline.core.data.model.product.ProductsWrapper;
import me.codeline.favoriteModule.util.RequestBody;
import me.codeline.library.core.manager.CLNetworkManager;
import me.codeline.library.core.network.CLGSonRequest;

public class FavoriteNetworkManager extends CLNetworkManager {

    public static final String HEADER_AUTHORIZATION = "Authorization";

    private HashMap<String, String> headers = new HashMap<>();

    private static final Object LOCK = new Object();
    private static FavoriteNetworkManager sInstance;



    /**
     * Constructor that initializes the cache manager
     *
     * @param context the application context
     */
    private FavoriteNetworkManager(Context context) {
        super(context);
    }

    public static FavoriteNetworkManager getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new FavoriteNetworkManager(context.getApplicationContext());
            }
        }
        return sInstance;
    }

    public CLGSonRequest<MessageWrapper> postFavorite(int productId, boolean status, final Listener<MessageWrapper> listener) {
        String url = ApiConstants.POST_FAVORITE + "/" + productId;
        JSONObject params = RequestBody.postFavorite(productId, status);
        return post(url, params, headers, listener, MessageWrapper.class);
    }

    public CLGSonRequest<ProductsWrapper> getFavorites(int page, Listener<ProductsWrapper> listener){
        String url = ApiConstants.GET_FAVORITES;
        JSONObject params = RequestBody.getFavorites(page);
        return get(url, params, headers, listener, ProductsWrapper.class);
    }

    public CLGSonRequest<MessageWrapper> deleteFavorites(List<Integer> ids, final Listener<MessageWrapper> listener) {
        String url = ApiConstants.DELETE_MULTIPLE_FAVORITES;
        JSONObject params = RequestBody.deleteFavorites(ids);
        return delete(url, params, headers, listener, MessageWrapper.class);
    }

    @Override
    public void onNoConnectionError(Request request) {
    }
}
