package me.codeline.favoriteModule.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import me.codeline.favoriteModule.R;
import me.codeline.favoriteModule.databinding.ActivityFavoritesBinding;
import me.codeline.core.data.model.response.ApiResponse;
import me.codeline.core.data.database.product.Product;
import me.codeline.favoriteModule.ui.adapter.FavoriteAdapter;
import me.codeline.favoriteModule.util.InjectorUtils;
import me.codeline.core.data.viewmodel.BaseViewModel;
import me.codeline.favoriteModule.data.viewmodel.FavoriteViewModel;
import me.codeline.library.core.widget.CLItemDecoration;
import me.codeline.library.list.activity.CLListActivity;
import me.codeline.library.list.adapter.CLListAdapter;

public class FavoritesActivity extends CLListActivity<Product> {

    private FavoriteViewModel viewModel;
    private ActivityFavoritesBinding binding;

    public static Intent newInstance(Context context) {
        return new Intent(context, FavoritesActivity.class);
    }

    @Override
    public CLListAdapter<Product> initAdapter() {
        return new FavoriteAdapter(new ArrayList<Product>());
    }

    @Override
    public RecyclerView.LayoutManager initLayoutManager() {
        return new LinearLayoutManager(context);
    }

    @Override
    public int getContentView() {
        return R.layout.activity_favorites;
    }

    @Override
    public boolean isDataBindingEnabled() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = (ActivityFavoritesBinding) getDataBinding();
        viewModel = InjectorUtils.provideFavoriteViewModel(this);
        binding.setModel(viewModel);

        setBackButtonEnabled(true);
        setTitle("Favorites");
        setOptionsMenu(R.menu.favorite);
        setupRecyclerViewItemDecoration();

        observeFavorite();
        observeApiState();
    }

    private void setupRecyclerViewItemDecoration() {
        binding.recyclerView.addItemDecoration(new CLItemDecoration(getResources()));
    }

    private void observeFavorite() {
        viewModel.getFavorites().observe(this, new Observer<List<Product>>() {
            @Override
            public void onChanged(List<Product> favorites) {
                viewModel.setShowEmptyState(false);
                if (viewModel.getPage() == 1) {
                    if (favorites != null && favorites.size() == 0) {
                        viewModel.setShowEmptyState(true);
                        viewModel.setHasMore(false);
                        adapter.hideFooterLoader();
                        showHideEditMenuItem(false);
                        updateItems(new ArrayList<Product>());
                    } else {
                        binding.recyclerView.scheduleLayoutAnimation();
                        updateItems(favorites);
                        showHideEditMenuItem(true);
                    }
                } else {
                    if (favorites != null && favorites.size() < 18) {
                        viewModel.setHasMore(false);
                        adapter.hideFooterLoader();
                    }
                    addItems(favorites);
                }
            }
        });
    }

    private void observeApiState() {
        viewModel.getApiState().observe(this, new Observer<BaseViewModel.ApiState>() {
            @Override
            public void onChanged(BaseViewModel.ApiState apiState) {
                if (apiState.isLoading() && getItemCount() == 0) {
                    vm.show(loader);
                    viewModel.setLoading(true);
                } else {
                    vm.hide(loader);
                    viewModel.setLoading(false);
                    setRefreshing(false);
                }
                if (apiState.getException() != null) {
                }
            }
        });
    }

    private void showHideEditMenuItem(boolean show) {
        getToolbar().getMenu().findItem(R.id.mi_edit).setVisible(show);
    }


    @Override
    public void onRefresh() {
        viewModel.refresh();
    }

    private boolean isEditMode() {
        return ((FavoriteAdapter) adapter).isEditMode();

    }

    private List<Integer> getCheckedPositions() {
        List<Integer> checked = new ArrayList<>();
        int index = 0;
        for (Product product : getItems()) {
            if (product.isChecked()) {
                checked.add(index);
            }
            index++;
        }
        return checked;
    }

    private List<Integer> getCheckedIds(List<Integer> positions) {
        List<Integer> ids = new ArrayList<>();
        for (Integer position : positions) {
            ids.add(getItem(position).getId());
        }
        return ids;
    }

    private void handleMenuItems() {

        if (getCheckedPositions().size() > 0) {
            showRemoveMenuItem(true);
        } else {
            showRemoveMenuItem(false);
        }
    }

    private void showRemoveMenuItem(boolean show) {
        Menu menu = getToolbar().getMenu();
        menu.findItem(R.id.mi_edit).setVisible(!show);
        menu.findItem(R.id.mi_remove).setVisible(show);
    }

    @Override
    public void itemClicked(View view, int position) {
        switch (view.getId()) {
        }
    }


    @Override
    public void onLoadMore() {
        if (viewModel.hasMore()) {
            viewModel.increasePage();
        } else {
            getAdapter().hideFooterLoader();
        }
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {

        }
        return super.onMenuItemClick(item);
    }

    private void callDeleteFavorites() {
        viewModel.setLoading(true);
        List<Integer> checked = getCheckedPositions();
        List<Integer> ids = getCheckedIds(checked);
        showRemoveMenuItem(false);
        viewModel.callDeleteFavorite(ids).observe(this, new Observer<ApiResponse<String>>() {
            @Override
            public void onChanged(ApiResponse<String> response) {
                if (response.isSuccess()) {
                    viewModel.setLoading(false);
                    if (adapter.getItemCount() == 0) {
                        viewModel.setShowEmptyState(true);
                        showHideEditMenuItem(false);
                    }
                } else {
                    viewModel.setLoading(false);
                }
            }
        });
        removeFavorites(checked);
    }

    private void removeFavorites(final List<Integer> checked) {

        ArrayList<Product> list = new ArrayList<>();
        for (int i = 0; i < checked.size(); i++) {
            list.add(getItem(checked.get(i)));
        }
        getAdapter().removeItems(list);

    }



}