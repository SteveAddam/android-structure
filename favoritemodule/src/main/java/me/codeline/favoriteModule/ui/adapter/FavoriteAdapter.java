package me.codeline.favoriteModule.ui.adapter;

import android.os.Bundle;

import java.util.List;

import me.codeline.favoriteModule.constant.Constants;
import me.codeline.core.data.database.product.Product;
import me.codeline.favoriteModule.ui.holder.FavoriteHolder;
import me.codeline.library.list.adapter.CLListAdapter;

public class FavoriteAdapter extends CLListAdapter<Product> {

    private boolean editMode = false;

    public FavoriteAdapter(List<Product> items) {
        super(items);
        enableInfiniteScrolling();
    }

    @Override
    public boolean isDataBindingEnabled() {
        return true;
    }

    public void setEditMode(boolean editMode){
        this.editMode = editMode;
        notifyDataSetChanged();
    }

    public void toggleEditMode(){
        setEditMode(!editMode);
    }

    public boolean isEditMode() {
        return editMode;
    }

    @Override
    public Bundle getExtras() {
        Bundle extras = new Bundle();
        extras.putBoolean(Constants.EXTRA_EDIT_MODE, editMode);
        return extras;
    }

    @Override
    public Class onCreateViewHolder(int viewType) {
        return FavoriteHolder.class;
    }
}
