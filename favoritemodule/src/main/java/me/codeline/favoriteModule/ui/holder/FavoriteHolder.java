package me.codeline.favoriteModule.ui.holder;

import android.view.View;

import androidx.databinding.ViewDataBinding;

import me.codeline.favoriteModule.constant.Constants;
import me.codeline.favoriteModule.databinding.ViewFavoriteBinding;
import me.codeline.core.data.database.product.Product;
import me.codeline.library.list.holder.CLHolder;

public class FavoriteHolder extends CLHolder<Product> {

    private ViewFavoriteBinding binding;

    private boolean editMode = false;

    public FavoriteHolder(ViewDataBinding dataBinding) {
        super(dataBinding);
    }

    @Override
    public void onHolderCreated(View itemView) {
        binding = (ViewFavoriteBinding) getDataBinding();


        setClick(binding.checkbox);
        // this listener responsible for adding and subtracting buttons with click span duration 0
        View.OnClickListener btnListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setClickSpanDuration(0);
                FavoriteHolder.super.onClick(v);
            }
        };
        // this listener responsible for cardView inside we handle the click span duration if edit mode is true span duration is 0 else 1000
        View.OnClickListener cardListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleClickSpanDuration();
                FavoriteHolder.super.onClick(v);
            }
        };

        binding.imageAdd.setOnClickListener(btnListener);
        binding.imageMinus.setOnClickListener(btnListener);
        binding.cardView.setOnClickListener(cardListener);
    }

    @Override
    public void bindData(final Product data) {
        editMode = getExtras().getBoolean(Constants.EXTRA_EDIT_MODE);
        handleCheckbox();
        binding.setEditable(editMode);
        binding.setListener(this);
        binding.setProduct(data);
        binding.executePendingBindings();
    }

    private void showCheckbox(final boolean show) {
        binding.motionLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (show) {
                    binding.motionLayout.transitionToEnd();
                } else {
                    binding.motionLayout.transitionToStart();
                }
            }
        }, 50 * getAdapterPosition());

    }

    private void handleCheckbox() {
        if (getExtras().getBoolean(Constants.EXTRA_EDIT_MODE)) {
            showCheckbox(true);
        } else {
            showCheckbox(false);
        }
        handleClickSpanDuration();
    }

    private void handleClickSpanDuration() {
        if (getExtras().getBoolean(Constants.EXTRA_EDIT_MODE)) {
            setClickSpanDuration(0);
        } else {
            setClickSpanDuration(1000);
        }
    }

}
