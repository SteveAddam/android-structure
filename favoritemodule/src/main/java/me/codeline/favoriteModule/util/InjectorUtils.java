package me.codeline.favoriteModule.util;

import android.app.Activity;
import android.content.Context;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import me.codeline.core.data.database.ApplicationDatabase;
import me.codeline.core.util.AppExecutors;
import me.codeline.favoriteModule.data.source.cache.CacheDataSource;
import me.codeline.favoriteModule.data.database.FavoriteDatabase;
import me.codeline.favoriteModule.data.repository.FavoriteCacheDataRepository;
import me.codeline.favoriteModule.data.repository.FavoriteRemoteRepository;
import me.codeline.favoriteModule.data.repository.FavoriteRepository;
import me.codeline.favoriteModule.manager.FavoriteNetworkManager;
import me.codeline.favoriteModule.data.source.cache.remote.RemoteDataSource;
import me.codeline.favoriteModule.data.viewmodel.FavoriteViewModel;
import me.codeline.favoriteModule.data.viewmodel.FavoriteViewModelFactory;

public class InjectorUtils {

    public static FavoriteRepository provideFavoriteRepository(Context context) {
        FavoriteRemoteRepository remoteDataSource = provideRemoteDataSource(context);
        FavoriteCacheDataRepository cacheDataSource = provideCacheDataSource(context);
        return FavoriteRepository.getInstance(cacheDataSource, remoteDataSource);
    }

    private static FavoriteCacheDataRepository provideCacheDataSource(Context context) {
        CacheDataSource cacheDataSource = provideCacheRepo(context);
        return FavoriteCacheDataRepository.getInstance(cacheDataSource);
    }

    private static FavoriteRemoteRepository provideRemoteDataSource(Context context) {
        RemoteDataSource remoteDataSource = provideRemoteRepo(context);
        return FavoriteRemoteRepository.getInstance(remoteDataSource);
    }

    private static RemoteDataSource provideRemoteRepo(Context context) {
        FavoriteNetworkManager favoriteNetworkManager = provideNetworkManager(context);
        AppExecutors executors = AppExecutors.getInstance();
        return RemoteDataSource.getInstance(favoriteNetworkManager, executors);
    }

    private static CacheDataSource provideCacheRepo(Context context) {
        ApplicationDatabase database = FavoriteDatabase.getInstance(context.getApplicationContext());
        AppExecutors executors = AppExecutors.getInstance();
        return CacheDataSource.getInstance(executors, database);
    }

    private static FavoriteNetworkManager provideNetworkManager(Context context) {
        return FavoriteNetworkManager.getInstance(context);
    }

    public static FavoriteViewModelFactory provideFavoritesViewModelFactory(Context context) {
        return new FavoriteViewModelFactory(context);
    }

    public static FavoriteViewModel provideFavoriteViewModel(Activity activity) {
        FavoriteViewModelFactory favoritesViewModelFactory = InjectorUtils.provideFavoritesViewModelFactory(activity.getApplicationContext());
        return ViewModelProviders.of((FragmentActivity) activity, favoritesViewModelFactory).get(FavoriteViewModel.class);
    }
}
