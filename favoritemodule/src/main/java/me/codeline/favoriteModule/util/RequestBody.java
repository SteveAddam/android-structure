package me.codeline.favoriteModule.util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class RequestBody {

    public static JSONObject postFavorite(int productId, boolean status) {
        JSONObject params = new JSONObject();
        try {
            params.put("product_id", productId);
            params.put("status", status);
            return params;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject getFavorites(int page) {
        JSONObject params = new JSONObject();
        try {
            params.put("page", page);
            return params;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject deleteFavorites(List<Integer> ids) {
        JSONObject params = new JSONObject();
        try {
            int index = 0;
            for (Integer integer : ids) {
                params.put("ids[" + index + "]", integer);
                index++;
            }
            return params;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
